export sl="en"
export tl="es"
export stanza_lang_code=$sl
export vocab_size=32000

# 1 for languages with a small character set
# 0.9995 for languages like Japanese or Chinese
export character_coverage=1


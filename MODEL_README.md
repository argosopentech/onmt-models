# English-Spanish

Trained on [OpenSubtitles](opus.nlpl.eu/OpenSubtitles.php), [ParaCrawl](http://opus.nlpl.eu/ParaCrawl.php), and [UNPC](http://opus.nlpl.eu/UNPC.php) parallel corpuses compiled by [Opus](http://opus.nlpl.eu/index.php)

Includes pretrained models from [Stanza](https://github.com/stanfordnlp/stanza/blob/master/LICENSE).

